console.log("hello from JS");

let assignmentNumber = 5;

let message = 'This is the message';

// assignmentNumber = assignmentNumber + 2;
console.log("Result of the operation: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of the operation: " + assignmentNumber);

assignmentNumber *= 4;
console.log("Result of the operation: " + assignmentNumber);

let value = 8;
// value += 15;
// value -= 5;
// value *= 2;
// value /= 2;
// console.log(value)

let x = 15;
let y = 10;

let sum = x + y;
console.log(sum);


let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let remainder = x % y;
console.log(remainder);


// let mdas = 1 + 2 -3 * 4 / 5
// console.log(mdas)

let pemdas = 1 + (2 -3) * (4 / 5)
console.log(pemdas)

let z = 1;
let preIncrement = ++z;
console.log(preIncrement);
console.log(z);

let postIncrement = z++;
console.log(postIncrement);
console.log(z);

let preDecrement = --z;
console.log(preDecrement);
console.log(z);

let postDecrement = z--;
console.log(postDecrement);
console.log(z);

let bagongValue = 3;
let newValue = bagongValue++;
console.log(newValue);
console.log(bagongValue);

let numberA = 6;
let numberB = '6';

console.log(typeof numberA);
console.log(typeof numberB);

let coercion = numberA + (numberB);
console.log(coercion);

let expressionC = 10 + true;
console.log(expressionC);

let a = true;
console.log(typeof a);
let b = 10;
console.log(typeof b);

let expressionD = true + true;
console.log(expressionD);

let expressionE = 10 + false;
console.log(expressionE);

let expressionF = true + false;
console.log(expressionF);

let expressionG = 8 + null;
console.log(expressionG);

let d = null;
console.log(typeof d);

let expressionH = "Batch145" + null;
console.log(expressionH);

//Conversion Rules:
 //1. If atleast one operand is an object, it will be converted into a primitive value/data type.
 //2. After conversion, if atleast 1 operand is a string data type, the 2nd operand is converted into another string to perform concatenation.
 //3. In other cases where both operands are converted to numbers then an arithmetic operation is executed.

 expressionH = 9 + undefined;
 console.log(expressionH);
 let e = undefined;
 console.log(typeof e);

 let name = 'Juan';


 console.log(1 == 1);
 console.log(1 == 2);
 console.log(1 == '1');
 console.log(1 == true);
 console.log(1 == false);
 console.log(name == 'Juan');
 console.log('Juan' == 'juan');
 // console.log('Juan' == Juan);

 console.log(1 != 1);
 console.log(1 != 2);
 console.log(1 != '1');
 console.log(0 != false);
 let juan = 'juan';
 console.log('juan' != juan);

 console.log(1 === 1);
 console.log(1 === '1');
 console.log(0 === false);

 console.log(1 !== 1);
 console.log(1 !== 2);
 console.log(1 !== '1');
 console.log(0 !== false);

 //Developer's tip: upon creating conditions or statement it is strongly recommended to use "strict" equality operators over "loose" equality operators because it will be easier for us to predetermine outcomes and results in any given scenario.

let priceA = 1800;
let priceB = 1450;

console.log(priceA < priceB);
console.log(priceA > priceB);

let  expressionI = 150 <= 150;
console.log(expressionI);

//Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. it is writing convention for developers to add a prefix of "is" or "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

isLegalAge = true;
isRegistered = false;

let isAllowedToVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? '+ isAllowedToVote);

let isAllowedForVaccination = isLegalAge || isRegistered;
console.log('Did the person pass? ' + isAllowedForVaccination);

let isTaken = true;
let isTalented = false;

console.log(!isTaken);
console.log(!isTalented);