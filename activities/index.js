console.log("Hello World");
let x = 7;
let y = 23;

let sum = x + y;
console.log('The sum of the two numbers is: ' + sum);
let difference = x - y;
console.log('The difference of the two numbers is: ' + difference);
let product = x * y;
console.log('The product of the two numbers is: ' + product);
let quotient = x / y;
console.log('The quotient of the two numbers is: ' + quotient);

let isSumGreaterThanDifference = sum > difference;
console.log ('The sum is greater than the difference: ' + isSumGreaterThanDifference)

let isProductAndQuotientPositive = product > 0 && quotient > 0;
console.log('The product and quotient are positive numbers: ' + isProductAndQuotientPositive)

let isNegative = sum < 0 || difference < 0 || product < 0 || quotient < 0;
console.log('One of the results is negative: ' + isNegative)

let isNotZero = sum !==0 && difference !==0 && product !==0 && quotient !==0;
console.log('All the results are not equal to zero: ' + isNotZero)





